<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Authors;

class AuthorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $authors = [
            ['author' => 'Yaşar Kemal', 'is_foreign' => 0],
            ['author' => 'Oğuz Atay', 'is_foreign' => 0],
            ['author' => 'Sabahattin Ali', 'is_foreign' => 0],
            ['author' => 'John Steinback', 'is_foreign' => 1],
            ['author' => 'Jose Mauro De Vasconcelos', 'is_foreign' => 1],
            ['author' => 'Hakan Mengüç', 'is_foreign' => 0],
            ['author' => 'Stephen Hawking', 'is_foreign' => 1],
            ['author' => 'Uğur Koşar', 'is_foreign' => 0],
            ['author' => 'Mehmet Yıldız', 'is_foreign' => 0],
            ['author' => 'Mert Arık', 'is_foreign' => 0],
            ['author' => 'Marcus Aurelius', 'is_foreign' => 1],
            ['author' => 'Michel de Montaigne', 'is_foreign' => 1],
            ['author' => 'George Orwell', 'is_foreign' => 1],
            ['author' => 'Peyami Safa', 'is_foreign' => 0],
        ];

        Authors::insert($authors);
    }
}
