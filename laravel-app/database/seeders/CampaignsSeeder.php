<?php

namespace Database\Seeders;

use App\Models\Campaigns;

use Illuminate\Database\Seeder;

class CampaignsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $campaigns = [
            ['type' => 1, 'authorId' => 3, 'categoryId' => 1, 'how_many_product' => 2, 'how_many_free' => 1],
            ['type' => 2, 'author_type' => 0, 'discount_rate' => 5.00],
            ['type' => 3, 'basket_total_price' => 200.00, 'discount_rate' => 5.00],
        ];

        foreach ($campaigns as $campaign) {
            Campaigns::create($campaign);
        }

    }
}
