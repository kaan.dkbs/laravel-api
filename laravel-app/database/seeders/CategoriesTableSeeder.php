<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Categories;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ['title' => 'Roman'],
            ['title' => 'Kişisel Gelişim'],
            ['title' => 'Bilim'],
            ['title' => 'Din Tasavvuf'],
            ['title' => 'Öykü'],
            ['title' => 'Felsefe'],
        ];

        Categories::insert($categories);
    }
}
