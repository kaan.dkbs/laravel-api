<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('type')->comment('parameter : 1, 2, 3');
            $table->unsignedBigInteger('authorId')->nullable();
            $table->unsignedBigInteger('categoryId')->nullable();
            $table->smallInteger('how_many_product')->nullable();
            $table->smallInteger('how_many_free')->nullable();
            $table->smallInteger('author_type')->nullable()->comment('0: local, 1: foreign');
            $table->decimal('basket_total_price', 8, 2)->nullable();
            $table->decimal('discount_rate', 8, 2)->nullable()->comment('columns author_type and basket_total_price use this space in common');
            $table->timestamps();

            $table->foreign('authorId')->references('id')->on('authors');
            $table->foreign('categoryId')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
