<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrdersItem extends Model
{
    use HasFactory;

    protected $table = 'orders_item';

    protected $fillable = [
        'orders_id',
        'product_id',
        'product_category',
        'author_name',
        'product_name',
        'product_price',
        'product_quantity',
        'quantity',
    ];


    public function order()
    {
        return $this->belongsTo(Orders::class);
    }

    public function product()
    {
        return $this->belongsTo(Products::class);
    }
}
