<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    use HasFactory;

    protected $table = 'orders';

    protected $fillable = [
        'user_id',
        'order_number',
        'customer_name',
        'customer_sur_name',
        'customer_email',
        'customer_phone',
        'customer_address',
        'total_price',
        'cargo_price',
        'discount_price',
        'without_discount_price',
        'campaign_text'
    ];


    public function orderItems()
    {
        return $this->hasMany(OrdersItem::class);
    }
}
