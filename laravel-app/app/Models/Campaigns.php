<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Campaigns extends Model
{
    use HasFactory;

    protected $table = 'campaigns';

    protected $fillable = [
        'id',
        'type',
        'authorId',
        'categoryId',
        'how_many_product',
        'how_many_free',
        'author_type',
        'baslet_total_price',
        'discount_rate',
        'created_at',
        'updated_at'
    ];

    protected $primaryKey = 'id';

    public function author()
    {
        return $this->belongsTo('App\Models\Users', 'authorId', 'id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Categories', 'categoryId', 'id');
    }
}
