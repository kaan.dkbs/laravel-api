<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'title',
        'description',
        'price',
        'category_id',
    ];

    protected $primaryKey = 'product_id';

    public function category()
    {
        return $this->belongsTo(Categories::class);
    }

    public function author()
    {
        return $this->belongsTo(Authors::class);
    }

    public function orderItems()
    {
        return $this->hasMany(OrdersItem::class);
    }
}
