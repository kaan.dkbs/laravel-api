<?php

namespace App\Repositories;

use App\Models\Orders;

class OrdersRepository implements OrdersRepositoryInterface
{

    public function save(Orders $order)
    {
        return $order->save();
    }

    public function getAll()
    {
        return Orders::with('orderItems')->get();
    }

    public function findById($id)
    {
        return Orders::with('orderItems')->find($id);
    }
}
