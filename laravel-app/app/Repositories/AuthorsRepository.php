<?php

namespace App\Repositories;

use App\Models\Authors;

class AuthorsRepository implements AuthorsRepositoryInterface
{
    public function find($authorId)
    {
        return Authors::where('id', $authorId)->get();
    }
}
