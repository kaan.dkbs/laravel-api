<?php

namespace App\Repositories;

use App\Models\Campaigns;

interface CampaignsRepositoryInterface
{
    public function save(Campaigns $campaign);

    public function findAll();
}
