<?php

namespace App\Repositories;

use App\Models\Products;

interface ProductsRepositoryInterface
{
    public function findWhere($id);

    public function findByProductId($productId);

    public function save(Products $product);
}
