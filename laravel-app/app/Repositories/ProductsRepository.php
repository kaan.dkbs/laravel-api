<?php

namespace App\Repositories;

use App\Models\Products;

class ProductsRepository implements ProductsRepositoryInterface
{
    public function findWhere($id)
    {
        return Products::Where('product_id', $id)->with('category')->with('author')->first();
    }

    public function findByProductId($productId)
    {
        return Products::whereIn('product_id', $productId)->with('category')->with('author')->get();
    }

    public function save(Products $product)
    {
        return $product->save();
    }

}
