<?php

namespace App\Repositories;

use App\Models\Categories;

interface CategoriesRepositoryInterface
{
    public function find($categoryId);
}
