<?php

namespace App\Repositories;

use App\Models\Orders;

interface OrdersRepositoryInterface
{

    public function save(Orders $order);

    public function getAll();

    public function findById($id);

}
