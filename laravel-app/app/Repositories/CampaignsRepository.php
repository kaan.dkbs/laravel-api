<?php

namespace App\Repositories;

use App\Models\Campaigns;

class CampaignsRepository implements CampaignsRepositoryInterface
{
    public function save(Campaigns $campaign)
    {
        return $campaign->save();
    }

    public function findAll()
    {
        return Campaigns::all();
    }
}
