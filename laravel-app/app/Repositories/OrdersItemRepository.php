<?php

namespace App\Repositories;

use App\Models\OrdersItem;

class OrdersItemRepository implements OrdersItemRepositoryInterface
{
    public function save(OrdersItem $orderItem)
    {
        return $orderItem->save();
    }

}
