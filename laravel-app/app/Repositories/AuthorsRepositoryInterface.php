<?php

namespace App\Repositories;

use App\Models\Authors;

interface AuthorsRepositoryInterface
{
    public function find($authorId);
}
