<?php

namespace App\Repositories;

use App\Models\Categories;

class CategoriesRepository implements CategoriesRepositoryInterface
{
    public function find($categoryId)
    {
        return Categories::where('id', $categoryId)->get();
    }
}
