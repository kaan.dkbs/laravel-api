<?php

namespace App\Repositories;

use App\Models\OrdersItem;

interface OrdersItemRepositoryInterface
{
    public function save(OrdersItem $orderItem);

}
