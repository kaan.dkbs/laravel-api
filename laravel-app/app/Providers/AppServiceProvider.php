<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\OrdersRepositoryInterface',
            'App\Repositories\OrdersRepository',
        );

        $this->app->bind(
            'App\Repositories\ProductsRepositoryInterface',
            'App\Repositories\ProductsRepository',
        );

        $this->app->bind(
            'App\Repositories\OrdersItemRepositoryInterface',
            'App\Repositories\OrdersItemRepository',
        );

        $this->app->bind(
            'App\Repositories\CampaignsRepositoryInterface',
            'App\Repositories\CampaignsRepository',
        );

        $this->app->bind(
            'App\Repositories\AuthorsRepositoryInterface',
            'App\Repositories\AuthorsRepository',
        );

        $this->app->bind(
            'App\Repositories\CategoriesRepositoryInterface',
            'App\Repositories\CategoriesRepository',
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
