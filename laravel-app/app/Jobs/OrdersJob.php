<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Config\Repository;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repositories\OrdersRepositoryInterface;
use App\Models\Orders;
use App\Repositories\OrdersItemRepositoryInterface;
use App\Models\OrdersItem;
use App\Repositories\ProductsRepositoryInterface;
use App\Repositories\CampaignsRepository;
use Illuminate\Support\Facades\Cache;

class OrdersJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $data;
    private $orderItems;

    public function __construct($data, $orderItems)
    {
        $this->data = $data;
        $this->orderItems = $orderItems;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(OrdersRepositoryInterface     $ordersRepository,
                           OrdersItemRepositoryInterface $ordersItemRepository,
                           ProductsRepositoryInterface   $productsRepository,
                           CampaignsRepository           $campaignsRepository)
    {

        $campaigns = $campaignsRepository->findAll();

        $authorType = [0 => 'Yerli', 1 => 'Yabancı'];

        $campaignId = null;
        $campaignDiscountPrice = 0;
        $campaignText = null;

        foreach ($this->orderItems as $item) {
            foreach ($campaigns as $campaign) {
                if ($campaign->id == 1) {
                    if (($item['author_id'] == $campaign->authorId) && ($item['product_category_id'] == $campaign->categoryId)) {
                        $discount = $item['product_price'] * $campaign->how_many_free;
                        if (($item['quantity'] >= $campaign->how_many_product) && ($discount > $campaignDiscountPrice)) {
                            $campaignDiscountPrice = $discount;
                            $campaignText = $item['author_name'] . ' yazarının ' . $item['product_category'] . ' kitaplarında ' . $campaign->how_many_product . ' üründen ' . $campaign->how_many_free . '  tanesi bedava kampanyası kullanılmıştır';
                        }
                    }
                } else if ($campaign->id == 2) {
                    if ($item['author_is_foreign'] == $campaign->author_type) {
                        $discountRate = number_format($campaign->discount_rate / 100, 4);
                        $discount = ($item['quantity'] * $item['product_price']) * $discountRate;
                        if ($discount > $campaignDiscountPrice) {
                            $campaignDiscountPrice = $discount;
                            $campaignText = $authorType[$campaign->author_type] . ' yazar kitaplarında %' . $campaign->discount_rate . ' indirim kampanyası kullanılmıştır';
                        }
                    }
                } else if ($campaign->id == 3 && ($this->data['totalPrice'] + $this->data['cargoPrice'] > $campaign['basket_total_price'])) {
                    $discountRate = number_format($campaign->discount_rate / 100, 4);
                    $discount = $this->data['totalPrice'] * $discountRate;
                    if ($discount > $campaignDiscountPrice) {
                        $campaignDiscountPrice = $discount;
                        $campaignText = 'Sepet toplamı ' . $campaign->basket_total_price . ' TL üzerindeki siparişlerde %' . $campaign->discount_rate . ' indirim kampanyası kullanılmıştır';
                    }
                }
            }
        }
        $order = new Orders();
        $order->user_id = $this->data['userId'];
        $order->order_number = $this->data['orderNumber'];
        $order->customer_name = $this->data['name'];
        $order->customer_sur_name = $this->data['surName'];
        $order->customer_email = $this->data['email'];
        $order->customer_phone = $this->data['phone'];
        $order->customer_address = $this->data['address'];
        $order->total_price = $this->data['totalPrice'] - $campaignDiscountPrice;
        $order->cargo_price = $this->data['cargoPrice'];
        $order->discount_price = $campaignDiscountPrice;
        $order->without_discount_price = $this->data['withoutDiscountPrice'];
        $order->campaign_text = $campaignText;

        if ($ordersRepository->save($order)) {
            foreach ($this->orderItems as $item) {
                $itemClient = new OrdersItem();
                $itemClient->orders_id = $order->id;
                $itemClient->product_id = $item['product_id'];
                $itemClient->product_name = $item['product_name'];
                $itemClient->product_category = $item['product_category'];
                $itemClient->author_name = $item['author_name'];
                $itemClient->product_price = $item['product_price'];
                $itemClient->quantity = $item['quantity'];
                if ($ordersItemRepository->save($itemClient)) {
                    $product = $productsRepository->findWhere($item['product_id']);
                    $product->stock_quantity = $product->stock_quantity - $item['quantity'];
                    $product->save();
                }
            }
            $order = $ordersRepository->findById($order->id);
            Cache::put('order_' . $order->order_number, $order, null);
        }
    }
}
