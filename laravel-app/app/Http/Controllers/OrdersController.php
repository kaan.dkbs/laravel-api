<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Repositories\ProductsRepositoryInterface;
use App\Repositories\CampaignsRepositoryInterface;
use App\Repositories\OrdersRepositoryInterface;
use App\Jobs\OrdersJob;
use Illuminate\Support\Facades\Cache;


class OrdersController extends Controller
{
    const CARGO_PRICE = 10.00;

    protected $ordersRepository;
    protected $productsRepository;
    protected $campaignsRepository;

    public function __construct(ProductsRepositoryInterface  $productsRepository,
                                CampaignsRepositoryInterface $campaignsRepository,
                                OrdersRepositoryInterface    $ordersRepository)
    {
        $this->ordersRepository = $ordersRepository;
        $this->productsRepository = $productsRepository;
        $this->campaignsRepository = $campaignsRepository;

    }

    public function createOrder(Request $request)
    {

        $data = $request->only(['name', 'surName', 'email', 'phone', 'address', 'products', 'products.id', 'products.quantity']);

        $validator = Validator::make($data, [
            'name' => 'required|string|max:255',
            'surName' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'phone' => ['required', function ($attribute, $value, $fail) {
                $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
                try {
                    $phoneNumber = $phoneUtil->parse($value, 'TR');
                    if (!$phoneUtil->isValidNumber($phoneNumber)) {
                        throw new \Exception();
                    }
                } catch (\Exception $e) {
                    $fail($attribute . ' is invalid.');
                }
            }],
            'address' => 'required|string|max:1000',
            'products' => 'required|array|min:1',
            'products.*.id' => 'required|integer|min:1',
            'products.*.quantity' => 'required|integer|min:1',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->all()], 400);
        }

        foreach ($data['products'] as $product) {
            $productIds[] = $product['id'];
            $quantities[] = $product['quantity'];
        }

        $products = $this->productsRepository->findByProductId($productIds);

        if ($products->count() != count($productIds)) {
            return response()->json(['message' => 'Product not found.'], 404);
        }

        $totalPrice = 0;
        $orderItems = [];
        foreach ($products as $product) {
            $key = array_search($product->product_id, $productIds);
            $orderItems[] = array(
                'product_id' => $product->product_id,
                'product_name' => $product->title,
                'product_category' => $product->category->title,
                'product_category_id' => $product->category->id,
                'author_name' => $product->author->author,
                'author_id' => $product->author->id,
                'author_is_foreign' => $product->author->is_foreign,
                'product_price' => $product->list_price,
                'quantity' => $quantities[$key]
            );

            if ($product->stock_quantity < $quantities[$key]) {
                return response()->json(['message' => 'Not enough stock for ' . $product->title . '.'], 400);
            }
            $totalPrice += $product->list_price * $quantities[$key];
        }


        $cargoPrice = self::CARGO_PRICE;
        if ($totalPrice > 50) {
            $cargoPrice = 0;
        }

        $orderNumber = date('YmdHis') . rand(1000, 9999);
        $data['orderNumber'] = $orderNumber;
        $data['userId'] = auth()->user()->id;
        $data['totalPrice'] = $totalPrice + $cargoPrice;
        $data['cargoPrice'] = $cargoPrice;
        $data['withoutDiscountPrice'] = $totalPrice;

        if (OrdersJob::dispatch($data, $orderItems)) {
            return response()->json(['status' => 'success', 'message' => 'Order created successfully.', 'order_number' => $orderNumber], 200);
        }

        return response()->json(['status' => 'error', 'message' => 'Order could not be created.'], 400);

    }

    public function getOrder(Request $request, $number)
    {
        $order = Cache::get('order_' . $number);
        if ($order) {
            return response()->json($order);
        }

        return response()->json(['message' => 'Order not found.'], 404);

    }

}
