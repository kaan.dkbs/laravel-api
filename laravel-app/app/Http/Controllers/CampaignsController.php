<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Repositories\CampaignsRepositoryInterface;
use App\Models\Campaigns;
use App\Repositories\AuthorsRepositoryInterface;
use App\Repositories\CategoriesRepositoryInterface;

class CampaignsController extends Controller
{
    protected $campaignsRepository;
    protected $authorsRepository;
    protected $categoriesRepository;

    public function __construct(CampaignsRepositoryInterface $campaignsRepository, AuthorsRepositoryInterface $authorsRepository, CategoriesRepositoryInterface $categoriesRepository)
    {
        $this->campaignsRepository = $campaignsRepository;
        $this->authorsRepository = $authorsRepository;
        $this->categoriesRepository = $categoriesRepository;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCampaign(Request $request)
    {
        $data = $request->only(['type', 'authorId', 'categoryId', 'howManyProduct', 'howManyFree', 'authorType', 'basletTotalPrice', 'discountRate']);
        $type = $data['type'];
        $validator = Validator::make($data, [
            'type' => 'required|numeric|in:1,2,3',
            'authorId' => $type == 1 ? 'required|numeric' : '',
            'categoryId' => $type == 1 ? 'required|numeric' : '',
            'howManyProduct' => $type == 1 ? 'required|numeric|min:2' : '',
            'howManyFree' => $type == 1 ? 'required|numeric|min:1' : '',
            'authorType' => $type == 2 ? 'required|numeric|in:0,1' : '',
            'basletTotalPrice' => $type == 3 ? 'required|numeric|min:1' : '',
            'discountRate' => $type == 3 || $type == 2 ? 'required|numeric|min:1' : ''
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $checkAuthor = $this->authorsRepository->find($data['authorId']);
        if (!$checkAuthor) {
            return response()->json(['message' => 'Invalid authorId'], 404);
        }

        $checkCategory = $this->categoriesRepository->find($data['categoryId']);
        if (!$checkCategory) {
            return response()->json(['message' => 'Invalid categoryId'], 404);
        }

        $campaign = new Campaigns();
        $campaign->type = $data['type'];
        if ($campaign->type == 1) {
            $campaign->authorId = $data['authorId'];
            $campaign->categoryId = $data['categoryId'];
            $campaign->how_many_product = $data['howManyProduct'];
            $campaign->how_many_free = $data['howManyFree'];
        } else if ($campaign->type == 2) {
            $campaign->author_type = $data['authorType'];
            $campaign->discount_rate = $data['discountRate'];
        } else if ($campaign->type == 3) {
            $campaign->basket_total_price = $data['basletTotalPrice'];
            $campaign->discount_rate = $data['discountRate'];
        }


        if ($this->campaignsRepository->save($campaign)) {
            return response()->json(['message' => 'Campaign created successfully'], 200);
        }

        return response()->json(['message' => 'Campaign could not be created'], 500);

    }

}
